<!--
author:   Axel Klinger

version:  0.1

language: de

comment:  Ein einfaches Beispiel für einen LiaScript Kurs
-->
# Dokumente mit LiaScript

## Getting Started

a) Projekt auf GitLab erstellen (3-5min)

!?[Einführung LiaScript](https://gitlab.com/axel-klinger/open-science-project/-/raw/main/images/intro.mp4)

* Vorbereitung: Account erstellen
* Projekt anlegen
* Dokument erstellen
* erste Überschriften und Absätze einfügen
* Raw-URL in LiaScript einsetzen
* Link in README eintragen

b) Weitere Elemente in Markdown (10-15min)

* Links
* Listen
* Bilder
* Videos
* Folien

c) Weitere Inhalte und komfortableres Arbeiten (30-60min)

* lokales Arbeiten mit Atom oder VSC
* Tabellen
* Formeln
* Quizzes
* Diagramme
* Metadaten
* weitere Features

d) Profis : Referenz

* Programmieraufgaben
* Narrator

## Arbeitsumgebung

## Artefakte

### Dokumentation

### Präsentationen

### Publikationen
