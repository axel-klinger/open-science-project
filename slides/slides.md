---
title: The Idea of an Open Science Project Template
author: https://orcid.org/0000-0001-6442-3510 # Axel Klinger
theme : solarized
#title: Vision für Open Science Projekte/Publikationen
#author: Axel Klinger
#date: 27. November 2021
revealOptions:
  transition: slide
  slideNumber: false
  history: true
---

## Open Science Project

### Projektskizze

**Axel Klinger**

*9. Februar 2022*

---

# Inhalt

- Status Quo
- Ziel
- Umfang
- Ansatz
- TODOs

---

# Status Quo

----

## Forschung ist intransparent

In vielen Fällen wird ein Forschungsvorhaben durchgeführt und am Ende ein Bericht geschrieben, aber die einzelnen Schritte bzw. Eingangsdaten und Ergebnisse sind nicht nachvollziehbar/transparent und eine Beteiligung ist nicht frühzeitig möglich.

**Dabei sind allgemein Fehler in Ansätzen umso günstiger zu beheben, je früher sie erkannt werden!**

----

## Publikationen sind statisch und Papier orientiert

* PDFs wurden im 20. Jahrhundert erfunden
* Smartphones + Tablets erst im 21. Jahrhundert
* Papier ist nicht dynamisch

----

## Auffindbarkeit

... ist abhängig von Plattformen und der Qualität der Metadaten ...

----

## fehlende Partizipationsmöglichkeiten

* wenn Forschungsvorhaben erst nach der Veröffentlichung der Ergebnisse diskutiert werden (s.o.), ...

----

## fehlende Maschinenlesbarkeit

* vorhandene Texte sind nicht semantisch ausgezeichnet
* Texte sind zu konfus für automatische Erschließung z.B. im ORKG

---

# Ziel

Bessere Publikationen zu erreichen, um

* Wissenschaft und Bildung transparent und nachnutzbar zu machen
* Wissen besser erschließen zu können
* Antworten auf aktuelle Fragen zu bekommen

----

## Infrastrukturen

Zahlreich vorhanden mit Repositorien für Daten, Publikationen und Lehrmaterialien und nicht zuletzt dem ORKG für die Strukturierung von Wissen.

Potenzial zur Verbesserung: Aufbau der Publikationen

* Texte lassen sich nicht vollautomatisch erschließen
* Publikationen, Daten und Software sind nicht oder nur lose verbunden
* Qualität von Inhalten und Metadaten sind nicht selten unzureichend

----

## Publikationsformen

* Projektarbeiten
* Abschlussarbeiten
* Artikel
* Anträge
* Berichte
* Lehrmaterialen

----

## Attraktiviere Formate

* eBooks bieten den Lesenden die Möglichkeit, Schriftart und Schriftgröße selbst zu bestimmen und funktionieren auf Smartphones wie Tablets oder Desktops in optimaler Form
* aus einfachen Formaten, wie z.B. Markdown, generierte HTML Seiten bieten die Möglichkeit, interaktive Inhalte einzubeziehen

----

## Bessere Auffindbarkeit

* inkludierte Metadaten bieten hohes Ranking bei Google
* erleichtern die Einreichung in Publikationsplattformen
* sorgen bei impliziter Kontrolle für bessere Qualität

----

## Höhere Qualität

- Unterstützung bei der Erzeugung
- Annahme durch standardisierte QA-Berichte

----

## Maschinenlesbarkeit

* Reintext
* semantische Anreicherung

----

## Nachnutzbarkeit

- per Clone
- Zitation über PIDs

----

## Transparenz

- Beteiligte
- Änderungen und Versionen

----

## Partizipation

- Anmerkungen (per Formular)
- Diskussionen (in Issues)
- Verbesserungsvorschläge (Pull Requests)
- Beteiligung (per Berechtigung)

---

# Ansatz

----

## Wahl der Mittel

- hier: GitLab Projekte + Templates mit CI

----

## Alternativen

* einiges kann bereits GoogleDocs in Bezug auf Versionierung und Partizipation
* Vorteile
  - synchrone Bearbeitung
  - einfache Bedienung (bedingt, wenn es um die Formatierung geht)
* Nachteile sind dabei
  - fehlende Erweiterbarkeit
  - keine Digitale Souveränität
  - Papier basierte Formate

----

## Umfang eines Projekts

- Beschreibung
- Berichte / Publikationen
- Präsentationen
- Bilder für Präsentationen und Publikationen
- Daten (in Datenrepositorien?)
- Software in sepraten Repositorien zur allgemeinen Nachnutzung
- Gesamtstruktur via Template (z.B. Hugo - austauschbar!)

----

## Struktur

```
+-project-ospt
  +-slides/     # reveal-md
    +-osc-22-slides.md
    +-osf-22-slides.md
  +-articles/   # GitLab Text - thesis, proposal, report
    +-osc-22-article.md  # incl. metadata.yaml?
  +-lectures/   # LiaScript
    +-osp-template.md   # incl. metadata.yaml?
  +-images/     # reusable in all artifacts
    +-example.png
  +-docs/       # Docsify
    +-osp.md
    +-...
  +-README.md
  +-metadata.yaml
```

----

## Features für Dokumente (Gesamtprojekt?)

* Versionierung
* Partizipation
* Formatierung
* Qualität
* Anreicherung
* Planung
* Dokumentation
* Automatisierung

---

# Offene Punkte

Was fehlt in dem Ansatz mit GitLb?

----

## Einfacher Editor


* synchrone Bearbeitung mit mehreren Personen
* ... integration von HedgeDoc ...

----

## Qualitätskontrolle

* ein QA Server wie Sonar
![sonar-md](../images/Sonar-md.png)

----

## Metadaten

* einfache und einheitliche Metadaten in Projekten und Publikationen

----

## Semantische Anreicherungen

* was braucht z.B. der ORKG, um automatisiert Publikationen erschließen zu können?
* wie können die Autoren bei der Erstellung unterstützt werden?

---

# Arbeitspakete

... leiten sich aus den offenen Punkten ab ...

---

# Fragen?

----

## Kontakt

* axel.klinger@tib.eu
