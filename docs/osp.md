<!--
author:   Axel Klinger

version:  0.1

language: de

comment:  Ein Beispiel für ein Open Science Projekt
-->
# Open Science Project

[Axel Klinger](https://orcid.org/0000-0001-6442-3510)

[Axel Klinger](https://github.com/axel-klinger)

[Axel Klinger](https://gitlab.com/axel-klinger)

Wie sollte ein Open Science Projekt aufgebaut sein?

a) es sollte alle Artefakte des Projekts enthalten, dazu zählen u.a.

* Projektbeschreibung / Antrag
* Präsentationen
* Publikationen mit ggf. Daten und Software
* Zwischen- und Abschlussberichte
* Tutorials für die Verwendung/den Einsatz der Ergebnisse
* Gesamtdokumentation des Projekts

b) es sollte öffentlich sein

* offen für Partizipation in der Projektlaufzeit
* Kommunikationskanäle mit dem Projektteam vorsehen
* Planung und Status von Aufgaben/Arbeitspaketen transparent machen

c) es sollte maschinenlesbar sein

* gute Metadaten enthalten
* semantische Auszeichnungen enthalten

**d) es sollte einfach sein**

* keine Hürde beim Einstieg
* ohne Programmierkenntnisse realisierbar

## Struktur

??[folien](https://axel-klinger.gitlab.io/open-science-project/#/4/4)


### Umfang

* Dokumente, Daten, Software, Personen, Events, Planung, Aufgaben, Dokumentation, … so weit es geht mit PIDs (mandatory!?!)

Wie bereits oben genannt und nicht zuletzt in den Diskussionen um Forschungsdatenmanagement thematisiert, besteht eine vollständige Publikation nach heutigen Vorstellungen nicht nur aus dem reinen Textdokument in Form eines PDFs, wie es bisher üblich war, sondern umfasst allein bezüglicher Artefakte auch Daten, Software und ggf. noch weitere Elemente. Neben den Artefakten spielen auch die Metadaten eine wesentliche Rolle für die Auffindbarkeit in einer stetig wachsenden Menge von Publikationen in allen Bereichen der Wissenschaft.



### Partizipation

* über Feedback Formular, Aufgaben, Pull Requests, …

Bisher war es so, dass man eine Publikation in einem Repository eingereicht hat, und anschließend irgendwo über die guten und schlechten Stellen darin berichtet wurde. Es fehlt damit jeglicher Rückkanal für konstruktives Feedback. In der Open Source Softwareentwicklung werden Anmerkungen zu einem Produkt (vergleichbar mit einer Publikation) in Form von Issues angelegt, die dann abgearbeitet und abgehakt werden können. In gleicher Weise könnten auch wissenschaftliche Arbeiten offen und nachhaltig verbessert werden.

## Artefakte

### Dokumentation

Die Dokumentation von Projekten erfolgt in der Regel in einer Plattform für die gemeinsame Bearbeitung von Dokumenten. Das kann ein Wiki wie Confluence sein oder ein Dokumentenserver wie Microsoft SharePoint.

GitLab bietet ebenfalls eine gemeinsame - wenn auch nicht synchrone - Bearbeitung von Dokumenten an, bietet aber aufgrund seiner flexiblen Mechanismen für Automatisierung einiges an Vorteilen, wenn um die dynamische Erweiterung von Features geht, die sich dann auch in Form von Templates nachnutzen lassen.

Die Dokumentation von OSP wurde daher in Markdown verfasst und mit LiaScript präsentiert.

### Publikationen

Klassische Publikationen sind in der Regel PDF-Dokumente, deren Metadaten in Repositorien oder auf Webseiten gespeichert sind.

Im Folgenden wird gezeigt, welche Vorteile andere (zusätzliche) Formate und die Inklusion er Metadaten in den Dokumenten mit sich bringen, und wie diese mit einfachen mitteln erstellt werden können.

Konkrete Beispiele

* Qualitätskontrolle via Sonar mit SCREENSHOT
* Metadaten in Header integrieren mit SCREENSHOT
* Format HTML, PDF, EBOOK mit SCREENSHOT
* Einfügen in Repository als short SCREENCAST

#### Beispiel

Beispiel

??[Beispiel]()


#### Format

* optimale Darstellung, interaktive Elemente und Bedienung auf allen Geräten - vom Smartphone über Tablet bis PC

PDFs sind eine Publikationsform des letzten Jahrhunderts! Smartphones und Tablets, die heutzutage zu den gängigen Lesegeräten zählen wurden erst Anfang des 21. Jahrhunderts erfunden und sind bei den heutigen Generationen die bevorzugten Interaktionsgeräte.

Publikationen in Form von PDFs sind in der Regel auf DIN A4 ausgerichtet und nicht für 6, 7, 8 oder 10" Geräte geeignet. Wer schon einmal versucht hat, ein PDF auf einem Smartphone zu lesen, weiß was ich meine.

Neuere offene Publikationsformen, wie z.B. eBooks bieten zahlreiche Vorteile:

* bei der Erstellung muss ich nicht auf Formatvorlagen achten und kann mich auf die Inhalte konzentrieren, da ich nur reine Textdokumente in Markdown, oder bei mehr Features in AsciiDoc oder LaTeX verfasse  
* die Leser können selbst wählen zwischen Schriftgröße, Schriftart, Hintergrund etc. und haben damit den für sie bestmöglichen Lesekomfort
* reine Textdokumente ohne Formatierung sind einfacher maschinenlesbar, wodurch sich auch einfacher Qualitätskontrollen umsetzen lassen (mehr dazu später ...)

#### Metadaten

* enthalten, geprüft und konform zu schema.org/CreativeWork

* PIDs sollten eine wesentliche Rolle in jeder Publikation spielen
  - Unterstützung bei der Erstellung

Beispiel: wenn die Metadaten bereits im Dokument in sauberer Form enthalten sind bringt das zahlreiche Vorteile, wie z.B.

* bei der Veröffentlichung in einem Repository reicht allein die URL/der DOI des Dokuments aus, um alle Metadaten automatisch zu füllen, sofern das Repository diese aus dem Header interpretieren kann (Beispiel twillo und "Gitlab für Texte")
* in der Google-Suche werden diese Publikationen besonders hoch geranked (Beispiel "Gitlab für Texte" oder "Studienarbeit Fassade")
* die Metadaten liegen in der Hand des Erstellers
* -> per QA und Formular (mit dahinter liegenden Terminologien) lassen sich die Metadaten allein aus einem Template benutzerfreundlich und standardisiert erstellen!

-> eine QA via CI im Template könnte auf einfache Weise sicher stellen, dass eine Publikation nur dann anerkannt wird, wenn z.B. alle Zitationen per DOI erfolgen, alle Autoren mit (gültigen!) ORCIDs angegeben sind, bestimmte Richtlinien erfüllt sind und sämtliche minimal aber geforderte Metadaten nach schema.og/CreativeWork vollständig gesetzt sind

**Unterstützung**

* wenn ich die ORCID eines Autors eingebe wird automatisch der Name (und was vielleiht noch?) eingefügt
  - Autoren sind die Contributoren des Dokuments -> wenn diese im GitLab Projekt eingetragen sind, können sie dann automatisch eingfügt werden? Kann ein GitLabb-User seine ORCID hinterlegen?
* wenn ich ein Fachgebiet angebe wird automatisch der passende Thesaurus(?) für die Vergabe von standardisierten Schlagworten geladen, die beim Tippen vervollständigt und mit Enter übernommen werden können
  - oder lassen sich diese schon aus dem Dokument extrahieren bzw. die Extraktion unterstützen?
* der Titel wird aus dem Titel des Projekts übernommen
* das Veröffentlichungsdatum wird aus dem Release/Tag übernommen
* die Institution wird pro Autor (wenn vorhanden aus dem ORCI gezogen)
* wie lassen sich die Namen/IDs von Veranstaltungen integrieren? über Tags?

#### Qualitätskontrolle

Die automatische/statische Qualitätskontrolle ermöglicht es, Autoren dabei zu unterstützen, bessere Dokumente zu verfassen, die sowohl für Mensch als auch Maschine besser lesbar und verständlich sind.

Bei der Qualitätskontrolle von Dokumenten kann bereits auf eine Vielzahl vorhandener Werkzeuge zugegriffen werden, welche sich in den QA Prozess integrieren lassen.

**Beispiele für Werkzeuge**

* statische Codeanalyse mit SonarQube angepasst auf Texte
* Linter für Editoren wie Atom oder Visual Studio Code
* Language Tool
* Projekte wie write-good oder academic-writing

**QA auf verschiedenen Ebenen**

* Textstruktur, wie richtige Überschriftsebenen
* einfache inhaltliche Regeln, wie zu lange Sätze
* syntaktische Regeln, wie "keine Passivsätze"
* ... und vielleicht auch semantische Regeln?

**Metadaten**

Sowohl auf Dokumentebene als auch auf Projektebene kann auch die Vollständigkeit der Metadaten im Projekt geprüft werden und Autoren können bei der Erstellung der Metadaten unterstützt werden.

---

Am Beispiel einer statischen Code Analyse in der Software Entwicklung:

**Vergleich einiger Regeln:**

Einfache Struktur bzw. Markdown Parser

* zu lange Methoden - zu lange (und damit potentiell unverständliche) Sätze
* leere Methoden - Überschriften ohne Absätze bzw. Abschnitte ohne Inhalte

Auf Basis eines Abstract Syntax Tree für die deutsche (und weitere) Sprache

* überlagerte Variablen - Mehrdeutigkeiten
* ... Zusammenhang des Textes durch Analyse des Kontextgraphen ...
* Regeln für gute Texte, wie z.B. https://www.google.com/search?q=Regeln+für+gute+texte
  - Subjekt und Verb nach vorne!
  - Nominalkonstruktionen vermeiden!
  - Passivsätze vermeiden. ...
  - Füllwörter vermeiden!
  - Schachtelsätze vermeiden.

Durch Validierung gegen Grundwortschatz und Fachwortschatz

* undefinierte Variablen - Begriffe, die weder im Grundwortschatz, noch im Fachvokabular definiert sind und nicht in der Form auftauchen "ein X ist ein ..."


**Unterstützung**

* Auswertungen mit Empfehlungen für Verbesserungen in Sinne einer statischen Codeanalyse wären ein erster, einfacher Schritt und können modular und einfach weiterentwickelt und erprobt werden
* langfristig ist dabei anzustreben, dass Autoren im Editor in Echtzeit Feedback bekommen, was sie verbessern können - je früher man einen Fehler entdeckt, desto günstiger ist er zu beheben! d.h. z.B. wenn ich einen zu langen Satz formuliere sollte ich idealerweise gleich im Editor sehen, dass er suboptimal ist -> über SonarLint sollte das bereits mit Verbindung zum SonarQube Server in VS Code möglich sein!

#### Semantische Anreicherung

* Wie sollten Publikationen aufgebaut/gestaltet werden, damit eine automatisierte Erschließung z.B. für den ORKG möglich ist?
* Wie können Autoren bei der Erstellung unterstützt werden?

## Veröffentlichung in Repo

Beispiel DSpace

* Einreichen des Gesamtprojekts per URL z.B. https://gitlab.com/gruppe/projekt
* Repo lädt Buildreport und prüft QA
* Repo zieht alle Metadaten vom Projekt und den enthaltenen Artefakten und indiziert sie
* Repo zieht alle Versionen und indiziert sie?
* in Detailseite lässt sich die Version auswählen und alle Artefakte werden angezeigt
* Repo aktualisiert regelmäßig die Metadaten und Versionen

## Präsentationen

Für einfache Präsentationen bietet sich reveal-md an. Die Präsentationen werden in Markdown geschrieben und können direkt im Browser gestartet werden. Das folgende Beispiel zeigt eine solche Präsentation. Mit der Taste F kann diese Präsentation im Vollbildmodus gestartet werden und mit Escape wird der Vollbildmodus wieder beendet.

??[folien](https://axel-klinger.gitlab.io/open-science-project/#/)

## Tutorials

Interaktive Tutorials, die über die üblichen Komponenten in Webseiten, wie Bilder, Videos, Formeln und Tabellen auch interaktive Quizzes oder ausführbare Code-Schnipsel enthalten können, lassen sich als reine Markdown-Dateien mit LiaScript ausführen. Dazu reicht es bereits aus, die URL einer Markdown-Datei auf https://liascript.github.io einzufügen.

Einzelne Kurseinheiten können, wie im folgenden Beispiel gezeigt, auf einer Seite zusammengefasst und beschrieben werden.

**LiaScript**

[preview-lia](https://axel-klinger.gitlab.io/open-science-project/tutorials/osp-tutorial.md)

**GitLab für Open Science Projekte**

[preview-lia](https://liascript.github.io/course/?https://api.allorigins.win/raw?url=https://gitlab.com/axel-klinger/gitlab-tutorial/-/raw/main/tutorial.md)

**GitLab für Texte**

[preview-lia](https://axel-klinger.gitlab.io/gitlab-for-documents/course.md)
